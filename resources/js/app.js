/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import swal from 'sweetalert2';
import Vue from 'vue';
window.Swal = swal;
// const toast= swal.mixin({
//     toast: true,
//     position: top-end,
//     showCancelButton: false,
//     timer: 3000
// });

// window.toast = toast;
window.Fire = new Vue();

import Form from 'vform';
import {
    HasError,
    AlertError,
  } from 'vform/src/components/bootstrap5'
  // 'vform/src/components/bootstrap4'
  // 'vform/src/components/tailwind'

window.Form = Form;

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Start Users Component
Vue.component('all-users-component', require('./components/users/AllUsersComponent.vue').default);

// End Users Component

// Start Roles Component
Vue.component('all-roles-component', require('./components/roles/AllRolesComponent.vue').default);

// End Roles Component


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
